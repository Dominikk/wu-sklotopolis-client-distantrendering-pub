## Distant Rendering for Wurm Unlimited

I finally found enough time to implement distant rendering into the Wurm Unlimited client. Therefore, I created a mod that enables distant tree rendering and so implemented a feature that has been requested a lot for the last years.  

This mod requires Ago's client modloader that can be downloaded from GitHub: https://github.com/ago1024/WurmClientModLauncher/releases
Simply put this mod into your Wurm Unlimited client mod folder like any other mod. **This mod only works on servers that are using the server side mod that improves the distant terrain generation.**  The server side mod can be found at the following URL: https://gitlab.com/Dominikk/wu-sklotopolis-distantrendering-server
**You are using this mod at your own risk!**  
  
Please report any found problems as a GitLab issue, I will look into them. In case you find any problem please include as many details as possible and screenshots of the problem.  
  

## Known Issues and Limitations:

 - The mod currently only works on servers that use the server side mod  that improves distant terrain generation. 
 - Every 125 tiles of movement there is a small lag spike on the client because the render origin is changed to shrink the vertices. 
 - Performance is good but not perfect small lag spikes can happen, I am testing with a Gefore GTX 1050Ti from 2017, getting around 50   FPS. Reduce the rendering range in the properties file if your FPS drop too low.
 - This mod will not run on the old potatoe you bought as a PC 10 years ago ![:)](https://storage.proboards.com/6367702/images/hXyXnlOEoieVpasvHLEs.png)  
  
  

## **(OPTIONAL) If you want to render really far**
 
This step is optional and solves a potential out of memory error. It only works on pretty decent hardware.  
Navigate to the root directory of the Wurm Unlimited Client (wurmlauncher) and check if you have the file "LaunchConfig.ini".  
Edit or create it and enter the following details:  
  

    [Memory]  
    InitialHeap=512m  
    MaxHeapSize=8192m

  

## Screenshots
![enter image description here](https://i.imgur.com/jho1jsi.png)

![enter image description here](https://i.imgur.com/AL5wg1p.jpeg)

![enter image description here](https://i.imgur.com/lBr0RWl.jpeg)
