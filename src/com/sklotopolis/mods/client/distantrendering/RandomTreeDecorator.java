package com.sklotopolis.mods.client.distantrendering;

import com.wurmonline.mesh.Tiles;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class RandomTreeDecorator {

    private final Random random;

    public RandomTreeDecorator(int x, int y) {
        random = new Random((long) x * y);
    }

    private Tiles.Tile getRandomTreeOrBush() {
        List<Tiles.Tile> vegetationTypes = Arrays.stream(Tiles.Tile.values()).filter(
                tile -> (tile.isTree() || tile.isBush()) && !tile.isMycelium() && !tile.isEnchanted() && tile != Tiles.Tile.TILE_TREE).collect(Collectors.toList());
        return vegetationTypes.get(random.nextInt(100) % vegetationTypes.size());
    }

    public Tiles.Tile getRandomVegetationOrGrass() {

        if (random.nextInt(100) < 75) {
            return Tiles.Tile.TILE_GRASS;
        }
        if (random.nextInt(100) < 20) {
            return getRandomTreeOrBush();
        }

        return null;
    }
}
