package com.sklotopolis.mods.client.distantrendering;

import com.wurmonline.client.WurmClientBase;
import com.wurmonline.client.game.PlayerPosition;
import com.wurmonline.client.game.World;
import com.wurmonline.client.renderer.Color;
import com.wurmonline.client.renderer.backend.Primitive;
import com.wurmonline.client.renderer.backend.RenderState;
import com.wurmonline.client.renderer.cell.Volume;
import com.wurmonline.client.renderer.terrain.weather.Weather;
import com.wurmonline.client.resources.textures.ResourceTexture;
import com.wurmonline.client.resources.textures.ResourceTextureLoader;
import com.wurmonline.client.util.GLHelper;
import com.wurmonline.mesh.Tiles;
import com.wurmonline.shared.util.TerrainUtilities;

import java.nio.FloatBuffer;
import java.util.Random;


public class DistantTreePosition implements Comparable<DistantTreePosition> {
    private static RenderState renderState;
    private static RenderState renderStateSolid;
    private static RenderState renderStateSolidAlpha;

    static {
        DistantTreePosition.renderState = new RenderState();
        DistantTreePosition.renderState.alphatest = Primitive.TestFunc.GREATEREQUAL;
        DistantTreePosition.renderState.alphaval = 0.7f;
        DistantTreePosition.renderState.depthwrite = true;
        DistantTreePosition.renderState.depthtest = Primitive.TestFunc.LESSEQUAL;
        if (GLHelper.useDeferredShading()) {
            DistantTreePosition.renderState.blendmode = Primitive.BlendMode.OPAQUE;
        } else {
            DistantTreePosition.renderState.blendmode = Primitive.BlendMode.ALPHABLEND;
        }
        DistantTreePosition.renderStateSolid = new RenderState();
        DistantTreePosition.renderStateSolid.alphatest = Primitive.TestFunc.GREATEREQUAL;
        DistantTreePosition.renderStateSolid.alphaval = 0.7f;
        DistantTreePosition.renderStateSolid.depthwrite = true;
        DistantTreePosition.renderStateSolid.depthtest = Primitive.TestFunc.LESSEQUAL;
        DistantTreePosition.renderStateSolid.blendmode = Primitive.BlendMode.ALPHABLEND;
        DistantTreePosition.renderStateSolidAlpha = new RenderState();
        DistantTreePosition.renderStateSolidAlpha.alphatest = Primitive.TestFunc.GREATEREQUAL;
        DistantTreePosition.renderStateSolidAlpha.alphaval = 0.7f;
        DistantTreePosition.renderStateSolidAlpha.depthwrite = true;
        DistantTreePosition.renderStateSolidAlpha.depthtest = Primitive.TestFunc.LESSEQUAL;
        DistantTreePosition.renderStateSolidAlpha.blendmode = Primitive.BlendMode.ALPHABLEND;
    }

    private final Color color;
    private final World world;
    private final Volume volume;
    private final Tiles.Tile tileType;
    private float x;
    private float height;
    private float y;
    private float xScale;
    private float hScale;
    private float texX;
    private float texY;
    private float texWidth;
    private float texHeight;
    private int gridXPos;
    private int gridYPos;
    private float treeImageWidth;
    private float treeImageHeight;
    private int distanceVertexOffsetH;

    DistantTreePosition(final World world, Tiles.Tile tileType) {
        this.gridXPos = 0;
        this.gridYPos = 0;
        this.treeImageWidth = 0.0f;
        this.treeImageHeight = 0.0f;
        this.distanceVertexOffsetH = 1;
        this.world = world;
        this.color = new Color();
        this.volume = new Volume();
        this.tileType = tileType;
    }

    boolean init(final Random random, final int xTile, final int yTile, final boolean force) {
        float treePositionX = (xTile + TerrainUtilities.getTreePosX(xTile, yTile)) * 4.0f;
        float treePositionY = (yTile + TerrainUtilities.getTreePosY(xTile, yTile)) * 4.0f;
        final float treePositionHeight = this.world.getDistantTerrainBuffer().getInterpolatedHeight(treePositionX, treePositionY);

        if (treePositionX == this.x && treePositionHeight == this.height && treePositionY == this.y && !force) {
            return false;
        }
        this.x = treePositionX;
        this.y = treePositionY;
        this.height = treePositionHeight;

        calculateColors(random);

        this.volume.setEmpty();
        int age = random.nextInt(16);

        calculateTreePositions();
        calculateTexValues();

        final float ageScale = (age + 1) / 16.0f;
        float scale = ageScale * 16.0f;
        if (tileType.isBush()) {
            scale *= 0.25f;
        }
        this.xScale = (random.nextFloat() * 0.2f - 0.1f + 1.0f) * scale;
        this.hScale = (random.nextFloat() * 0.2f - 0.1f + 1.0f) * scale;

        return true;
    }

    private void calculateTexValues() {
        if (tileType.isBush()) {
            this.distanceVertexOffsetH = 0;
        } else {
            this.distanceVertexOffsetH = 1;
        }
        this.texWidth = 0.25f;
        this.texHeight = 0.25f;

        this.texX = this.gridXPos * this.texWidth;
        this.texY = this.gridYPos * this.texHeight;
    }

    private void calculateTreePositions() {
        this.gridXPos = tileType.getTexturePosX((byte) 0);
        this.gridYPos = tileType.getTexturePosY((byte) 0);
        this.treeImageWidth = tileType.getTreeImageWidth((byte) 0);
        this.treeImageHeight = tileType.getTreeImageHeight((byte) 0);
    }

    private void calculateColors(Random random) {
        final float br = random.nextFloat() * 0.3f + 0.7f;
        this.color.r = br;
        this.color.g = br;
        this.color.b = br;

        if (tileType.isMycelium()) {
            final Color color = this.color;
            color.r *= 0.6f;
            final Color color2 = this.color;
            color2.g *= 0.5f;
            final Color color3 = this.color;
            color3.b *= 0.4f;
        }
        if (tileType.isEnchanted()) {
            this.color.r = 1.0f;
            this.color.g = 1.0f;
            this.color.b = 1.0f;
        }
    }

    int prepareDistantTree(final FloatBuffer vdata) {
        final float width = this.xScale / this.treeImageWidth;
        final float height = this.hScale / this.treeImageHeight;
        final float xDelta = this.world.getPlayerPosX() - this.x;
        final float yDelta = this.world.getPlayerPosY() - this.y;
        final float hypotenuse = (float) Math.sqrt(xDelta * xDelta + yDelta * yDelta);
        final float aSin = yDelta / hypotenuse;
        final float aCos = xDelta / hypotenuse;
        final float xa = -aSin * width / 2.0f;
        final float ya = aCos * width / 2.0f;
        final float[] ver = {this.x + xa - this.world.getRenderOriginX(), this.height - this.distanceVertexOffsetH, this.y + ya - this.world.getRenderOriginY(), this.x - xa - this.world.getRenderOriginX(), this.height - this.distanceVertexOffsetH, this.y - ya - this.world.getRenderOriginY(), this.x - xa - this.world.getRenderOriginX(), this.height - this.distanceVertexOffsetH + height, this.y - ya - this.world.getRenderOriginY(), this.x + xa - this.world.getRenderOriginX(), this.height - this.distanceVertexOffsetH + height, this.y + ya - this.world.getRenderOriginY()};
        final float[] tex = {this.texX, this.texY + this.texHeight, this.texX + this.texWidth, this.texY + this.texHeight, this.texX + this.texWidth, this.texY, this.texX, this.texY};
        final float[] nor = {0.0f, 0.8f, 0.0f};
        final float[] col = {this.color.r, this.color.g, this.color.b, 1.0f};
        vdata.put(ver, 0, 3);
        vdata.put(nor);
        vdata.put(col);
        vdata.put(tex, 0, 2);
        vdata.put(ver, 3, 3);
        vdata.put(nor);
        vdata.put(col);
        vdata.put(tex, 2, 2);
        vdata.put(ver, 6, 3);
        vdata.put(nor);
        vdata.put(col);
        vdata.put(tex, 4, 2);
        vdata.put(ver, 0, 3);
        vdata.put(nor);
        vdata.put(col);
        vdata.put(tex, 0, 2);
        vdata.put(ver, 6, 3);
        vdata.put(nor);
        vdata.put(col);
        vdata.put(tex, 4, 2);
        vdata.put(ver, 9, 3);
        vdata.put(nor);
        vdata.put(col);
        vdata.put(tex, 6, 2);
        return 6;
    }

    Volume getVolume() {
        return this.volume;
    }

    @Override
    public int compareTo(final DistantTreePosition o1) {
        final PlayerPosition pos = this.world.getPlayer().getPos();
        final float dist = this.volume.getDistance(pos.getX(), pos.getH(), pos.getY()) - o1.volume.getDistance(pos.getX(), pos.getH(), pos.getY());
        return Float.compare(dist, 0.0f);
    }

    public Tiles.Tile getTileType() {
        return tileType;
    }
}
