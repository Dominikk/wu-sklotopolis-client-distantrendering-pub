package com.sklotopolis.mods.client.distantrendering;

import com.wurmonline.client.game.DistantTerrainDataBuffer;
import com.wurmonline.client.game.World;
import com.wurmonline.client.renderer.Material;
import com.wurmonline.client.renderer.MaterialInstance;
import com.wurmonline.client.renderer.backend.Primitive;
import com.wurmonline.client.renderer.backend.Queue;
import com.wurmonline.client.renderer.backend.RenderState;
import com.wurmonline.client.renderer.backend.VertexBuffer;
import com.wurmonline.client.renderer.cell.Volume;
import com.wurmonline.client.util.GLHelper;
import com.wurmonline.mesh.Tiles;

import javax.xml.soap.Text;
import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import static com.sklotopolis.mods.client.distantrendering.DistantRenderingMod.MAX_RENDER_DISTANCE;

public class DistantTreeCell {

    private static MaterialInstance materialTreeDeferred;
    private static MaterialInstance materialReflection;

    static {
        if (GLHelper.useDeferredShading()) {
            DistantTreeCell.materialTreeDeferred = Material.load("material.gbuffer.static").instance();
            DistantTreeCell.materialReflection = Material.load("material.forward_dirlight", "#define PLANE_REFLECTION").instance();
        }
    }

    private int xCellPos;
    private int yCellPos;
    private World world;
    private Volume volume;
    private Random random;
    private int updateDelay;
    private boolean treeVertexOutdated;
    private boolean treeDataOutdated;
    private Volume treeVolume;
    private boolean treeVolumeOutdated;
    private Map<Long, DistantTreePosition> trees;
    private DistantTreePosition[] treesSorted;
    private boolean treesSortedOutdated;
    private VertexBuffer vbo;
    private boolean initialized;
    private int requiredVertex;
    private int allocatedVertex;
    private int treeVertex;
    private int bushVertex;
    private RenderState billboardState;
    private boolean previousRender;
    private boolean refresh;

    public DistantTreeCell(final World world) {
        initialiseVariables(world);
    }

    private void initialiseVariables(World world) {
        this.refresh = false;
        this.previousRender = true;
        this.world = world;
        this.treeVolume = new Volume();
        this.trees = new LinkedHashMap<>();
        this.treesSorted = new DistantTreePosition[0];
        this.treesSortedOutdated = false;
        this.vbo = null;
        this.initialized = false;
        this.requiredVertex = 0;
        this.allocatedVertex = 0;
        this.treeVertex = 0;
        this.bushVertex = 0;
        this.billboardState = new RenderState();
        this.billboardState.alphatest = Primitive.TestFunc.GREATEREQUAL;
        this.billboardState.alphaval = 0.7f;
        this.billboardState.depthwrite = true;
        this.billboardState.depthtest = Primitive.TestFunc.LESSEQUAL;
        if (GLHelper.useDeferredShading()) {
            this.billboardState.blendmode = Primitive.BlendMode.OPAQUE;
        } else {
            this.billboardState.blendmode = Primitive.BlendMode.ALPHABLEND;
        }
        this.random = new Random();
        this.volume = new Volume();
        this.updateDelay = this.random.nextInt(30) + 5;
    }

    private void prepareTreeBuffers() {
        final int treeCount = this.trees.values().size();
        if (treeCount == 0) {
            this.clearBuffers();
            return;
        }
        this.requiredVertex = treeCount * 6 * 6;
        if (this.initialized && (this.requiredVertex > this.allocatedVertex || 2 * this.requiredVertex < this.allocatedVertex)) {
            this.clearBuffers();
        }
        if (!this.initialized) {
            initialiseVertex();
        }
    }

    private void initialiseVertex() {
        this.vbo = VertexBuffer.create(VertexBuffer.Usage.VEGETATION, this.requiredVertex, true, false, true, true, false, 2, 0, false, true);
        this.allocatedVertex = this.requiredVertex;
        this.treeVertex = 0;
        this.bushVertex = 0;
        this.initialized = true;
        this.treeVertexOutdated = true;
    }

    private void rebuildTreeArray(final boolean force) {
        final int xCellStartTile = this.xCellPos * 16;
        final int yCellStartTile = this.yCellPos * 16;
        final DistantTerrainDataBuffer distantTerrainBuffer = this.world.getDistantTerrainBuffer();
        this.treeVolume.setEmpty();

        generateTrees(force, xCellStartTile, yCellStartTile, distantTerrainBuffer);

        final int treeCount = this.trees.values().size();
        this.treesSorted = new DistantTreePosition[treeCount];
        this.trees.values().toArray(this.treesSorted);
        this.treeDataOutdated = false;
        this.treeVolumeOutdated = true;
        this.prepareTreeBuffers();
    }

    private void generateTrees(boolean force, int xCellStartTile, int yCellStartTile, DistantTerrainDataBuffer distantTerrainBuffer) {
        RandomTreeDecorator randomTreeGenerator = new RandomTreeDecorator(xCellStartTile, yCellStartTile);
        for (int tileX = xCellStartTile; tileX < xCellStartTile + 16; ++tileX) {
            for (int tileY = yCellStartTile; tileY < yCellStartTile + 16; ++tileY) {
                final Long key = (long) tileX << 32 | (long) tileY;
                DistantTreePosition tree = this.trees.get(key);
                Tiles.Tile tileType = distantTerrainBuffer.getTileType(tileX, tileY);
                if (tileType.isTree() || tileType.isBush()) {
                    Tiles.Tile randomTile = randomTreeGenerator.getRandomVegetationOrGrass();
                    if (randomTile != null && distantTerrainBuffer.getInterpolatedHeight(tileX * 4, tileY * 4) > 5.0f) {
                        tileType = randomTile;
                    } else if (distantTerrainBuffer.getInterpolatedHeight(tileX * 4, tileY * 4) <= 5.0f) {
                        tileType = Tiles.Tile.TILE_GRASS;
                    }

                    if (tree == null) {
                        tree = new DistantTreePosition(this.world, tileType);
                        this.trees.put(key, tree);
                        this.treeVertexOutdated = true;
                    }
                    this.random.setSeed(distantTerrainBuffer.getRandomSeed(tileX, tileY));
                    if (tree.init(this.random, tileX, tileY, force)) {
                        this.treeVertexOutdated = true;
                    }
                } else if (tree != null) {
                    this.trees.remove(key);
                    this.treeVertexOutdated = true;
                }
            }
        }
    }

    private void rebuildTreeVolume() {
        this.treeVolume.setEmpty();
        for (final DistantTreePosition tree : this.trees.values()) {
            this.treeVolume.growToFit(tree.getVolume());
        }
        this.volume.growToFit(this.treeVolume);
        this.treeVolumeOutdated = false;
    }

    private void prepareDistantTrees() {
        final FloatBuffer vbuffer = (this.vbo != null) ? this.vbo.lock() : null;
        if (vbuffer != null) {
            this.treeVertex = 0;
            for (final DistantTreePosition tree : this.treesSorted) {
                if (tree.getTileType().isTree()) {
                    this.treeVertex += tree.prepareDistantTree(vbuffer);
                }
            }
            this.bushVertex = 0;
            for (final DistantTreePosition tree : this.treesSorted) {
                if (tree.getTileType().isBush()) {
                    this.bushVertex += tree.prepareDistantTree(vbuffer);
                }
            }
        }
        if (this.vbo != null) {
            this.vbo.unlock();
        }
        this.treeVertexOutdated = false;
    }

    private void updateTrees() {
        if (!shouldRenderTrees()) {
            previousRender = false;
            this.cleanup();
            return;
        }

        if (!previousRender) {
            previousRender = true;
            setOutdated();
        }

        boolean modified = false;
        if (this.treeDataOutdated) {
            this.rebuildTreeArray(false);
            modified = true;
        }
        if (this.treeVolumeOutdated) {
            this.rebuildTreeVolume();
            modified = true;
        }
        if (modified) {
            this.prepareTreeBuffers();
        }
        if (this.treeVertexOutdated) {
            this.prepareDistantTrees();
        }
    }

    private void renderDistantTrees(final Queue queue) {
        if (!this.initialized) {
            return;
        }
        if (this.treeVertex == 0 && this.bushVertex == 0) {
            return;
        }
        if (this.treeVertex > 0) {
            createTreeVertex(queue);
        }
        if (this.bushVertex > 0) {
            createBushVertex(queue);
        }
    }

    private void createTreeVertex(Queue queue) {
        final Primitive prim = queue.reservePrimitive();
        prim.type = Primitive.Type.TRIANGLES;
        prim.vertex = this.vbo;
        prim.index = null;
        prim.offset = 0;
        prim.num = this.treeVertex / 3;
        prim.texture[0] = TextureManager.treeTexture;
        prim.texture[1] = null;
        prim.texenv[0] = Primitive.TexEnv.MODULATE;
        prim.texturematrix = null;
        prim.setColor(null);
        if (queue.isDeferred() && DistantTreeCell.materialTreeDeferred != null) {
            prim.materialInstance = DistantTreeCell.materialTreeDeferred;
            prim.program = DistantTreeCell.materialTreeDeferred.getProgram();
            prim.bindings = DistantTreeCell.materialTreeDeferred.getProgramBindings();
        } else if (queue.isReflection()) {
            prim.materialInstance = DistantTreeCell.materialReflection;
            prim.program = DistantTreeCell.materialReflection.getProgram();
            prim.bindings = DistantTreeCell.materialReflection.getProgramBindings();
        }
        prim.lightManager = null;
        prim.copyStateFrom(this.billboardState);
        queue.queue(prim, null);
    }

    private void createBushVertex(Queue queue) {
        final Primitive prim = queue.reservePrimitive();
        prim.type = Primitive.Type.TRIANGLES;
        prim.vertex = this.vbo;
        prim.index = null;
        prim.offset = this.treeVertex;
        prim.num = this.bushVertex / 3;
        prim.texture[0] = TextureManager.bushTexture;
        prim.texture[1] = null;
        prim.texenv[0] = Primitive.TexEnv.MODULATE;
        prim.texturematrix = null;
        prim.setColor(null);
        if (queue.isDeferred() && DistantTreeCell.materialTreeDeferred != null) {
            prim.materialInstance = DistantTreeCell.materialTreeDeferred;
            prim.program = DistantTreeCell.materialTreeDeferred.getProgram();
            prim.bindings = DistantTreeCell.materialTreeDeferred.getProgramBindings(prim.program);
        } else if (queue.isReflection()) {
            prim.materialInstance = DistantTreeCell.materialReflection;
            prim.program = DistantTreeCell.materialReflection.getProgram();
            prim.bindings = DistantTreeCell.materialReflection.getProgramBindings();
        }
        prim.copyStateFrom(this.billboardState);
        queue.queue(prim, null);
    }

    public void renderTrees(final Queue queue) {
        if (shouldRenderTrees()) {
            this.renderDistantTrees(queue);
        }
    }

    public void setOutdated() {
        this.treeVertexOutdated = true;
        this.treeDataOutdated = true;
    }

    public void cleanup() {
        this.trees.clear();
        this.treesSorted = new DistantTreePosition[0];
        this.clearBuffers();
    }

    private void clearBuffers() {
        this.initialized = false;
        this.allocatedVertex = 0;
        this.treeVertex = 0;
        this.bushVertex = 0;
        if (this.vbo != null) {
            this.vbo.delete();
            this.vbo = null;
        }
    }

    public void refresh() {
        if (shouldRenderTrees()) {
            this.refresh = true;
        }
    }

    public void tick() {
        if (!shouldRenderTrees() || !isTerrainDataForCellLoaded()) {
            this.cleanup();
            previousRender = false;
            return;
        }

        if (this.treesSortedOutdated) {
            this.treesSortedOutdated = false;
            Arrays.sort(this.treesSorted);
        }

        if (this.updateDelay <= 0) {
            if (this.refresh) {
                this.rebuildTreeArray(true);
                this.refresh = false;
            }
            this.updateTrees();
            this.updateDelay = this.random.nextInt(30) + 5;
        } else {
            --this.updateDelay;
        }
    }

    private boolean shouldRenderTrees() {
        int xTileCell = (this.xCellPos + 1) * 16;
        int yTileCell = (this.yCellPos + 1) * 16;

        return (Math.min(this.world.getWorldSize(), this.world.getPlayerCurrentTileX() + 128) < xTileCell || Math.max(0, this.world.getPlayerCurrentTileX() - 128) >= xTileCell ||
                Math.min(this.world.getWorldSize(), this.world.getPlayerCurrentTileY() + 128) < yTileCell || Math.max(0, this.world.getPlayerCurrentTileY() - 128) >= yTileCell) &&
                Math.min(this.world.getWorldSize(), this.world.getPlayerCurrentTileX() + MAX_RENDER_DISTANCE) > xTileCell && Math.max(0, this.world.getPlayerCurrentTileX() - MAX_RENDER_DISTANCE) < xTileCell &&
                Math.min(this.world.getWorldSize(), this.world.getPlayerCurrentTileY() + MAX_RENDER_DISTANCE) > yTileCell && Math.max(0, this.world.getPlayerCurrentTileY() - MAX_RENDER_DISTANCE) < yTileCell;
    }

    public void setCellPosition(final int newX, final int newY) {
        this.xCellPos = newX;
        this.yCellPos = newY;
        this.cleanup();
        this.setOutdated();
    }

    private boolean isTerrainDataForCellLoaded() {
        final DistantTerrainDataBuffer distantTerrainBuffer = this.world.getDistantTerrainBuffer();
        final int xCellStart = this.xCellPos * 16;
        final int yCellStart = this.yCellPos * 16;
        Tiles.Tile type = distantTerrainBuffer.getTileType(xCellStart, yCellStart);
        float height = distantTerrainBuffer.getInterpolatedHeight(xCellStart * 4, yCellStart * 4);
        return type != Tiles.Tile.TILE_DIRT && height != -1.0f;
    }
}
