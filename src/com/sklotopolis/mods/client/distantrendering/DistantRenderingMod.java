package com.sklotopolis.mods.client.distantrendering;

import com.wurmonline.client.game.World;
import com.wurmonline.client.renderer.backend.Queue;
import com.wurmonline.client.renderer.gui.HeadsUpDisplay;
import javassist.*;
import javassist.bytecode.Descriptor;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;
import org.gotti.wurmunlimited.modloader.callbacks.CallbackApi;
import org.gotti.wurmunlimited.modloader.classhooks.HookException;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.interfaces.Configurable;
import org.gotti.wurmunlimited.modloader.interfaces.Initable;
import org.gotti.wurmunlimited.modloader.interfaces.WurmClientMod;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DistantRenderingMod implements WurmClientMod, Initable, Configurable {
    public final static String RENDER_DISTANCE_COMMAND = "render_distance";
    private static final Logger logger = Logger.getLogger(DistantRenderingMod.class.getName());
    public static int MAX_RENDER_DISTANCE;
    private static List<DistantTreeCell> distantAreaCells;
    private HeadsUpDisplay headupDisplay;

    private static String VERSION = "1.3-RELEASE";

    private static ReadWriteLock lock = new ReentrantReadWriteLock();

    @Override
    public void configure(Properties properties) {
        credits();
        MAX_RENDER_DISTANCE = Integer.parseInt(properties.getProperty("maxRenderDistance", "500"));
        logger.info("Render distance for distant trees: " + MAX_RENDER_DISTANCE);
    }

    private void credits() {
        logger.info("=== ATTENTION ===");
        logger.info("This mod has been developed by the Sklotopolis development team.");
        logger.info("Wurm Unlimited is a game produced by Code Club AB. Wurm Unlimited and Code Club AB do not endorse and are not responsible or liable for any content, products, services or information available within this mod.");
        logger.info("This mod is not connected to Wurm Unlimited. Please visit the Wurm Unlimited Official Site at www.wurmonline.com.");
        logger.info("Use this mod at your own risk, we are not responsible for any damage to your system.");
        logger.info("Distant rendering mod Version: " + VERSION);
    }

    @Override
    public void init() {
        try {
            ClassPool classPool = ClassPool.getDefault();
            CtClass ctCellRenderer = classPool.get("com.wurmonline.client.renderer.cell.CellRenderer");

            CtClass ctWorldRenderer = classPool.get("com.wurmonline.client.renderer.WorldRender");
            HookManager.getInstance().addCallback(ctWorldRenderer, "distantrenderer", this);

            CtClass ctWurmConsole = classPool.getCtClass("com.wurmonline.client.console.WurmConsole");
            HookManager.getInstance().addCallback(ctWurmConsole, "distantrenderer", this);

            CtClass ctVegetationRenderer = null;
            for (CtClass c : ctCellRenderer.getDeclaredClasses()) {
                if (c.getName().contains("CellRenderer$VegetationRenderer")) {
                    ctVegetationRenderer = c;
                    break;
                }
            }

            if (ctVegetationRenderer == null) {
                throw new HookException("Could not find VegetationRenderer inner class in CellRenderer class");
            }
            HookManager.getInstance().addCallback(ctVegetationRenderer, "distantrenderer", this);

            HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.HeadsUpDisplay", "init", "(II)V", () -> (proxy, method, args) ->
            {
                Object result = method.invoke(proxy, args);
                headupDisplay = (HeadsUpDisplay) proxy;
                return result;
            });

            modifyVegetationRenderer(ctVegetationRenderer);
            modifyWorldRenderer(ctWorldRenderer);
            modifyCellRenderer();
            modifyWurmConsole(ctWurmConsole);

        } catch (NotFoundException | CannotCompileException e) {
            logger.log(Level.SEVERE, e.getMessage() + " " + e);
            throw new HookException("Could not inject distant rendering. " + e);
        }
    }

    private void modifyWurmConsole(CtClass ctWurmConsole) throws NotFoundException, CannotCompileException {
        ctWurmConsole.getMethod("handleDevInput", "(Ljava/lang/String;[Ljava/lang/String;)Z")
                .insertBefore("if (distantrenderer.changeRenderDistance($1,$2)) return true;");
    }

    private void modifyVegetationRenderer(CtClass ctVegetationRenderer) throws NotFoundException, CannotCompileException {
        ctVegetationRenderer.getDeclaredMethod("execute")
                .instrument(new ExprEditor() {
                    @Override
                    public void edit(MethodCall m) throws CannotCompileException {
                        if (m.getMethodName().equals("setSecondaryLightManager")) {
                            String replaceString = "{ ";

                            replaceString += ""
                                    + "distantrenderer.setupDistantVegetationCells(this$0.world);"
                                    + "distantrenderer.renderDistantTrees(queue);"
                                    + "$_ = $proceed($$);"
                                    + "}";

                            m.replace(replaceString);
                        }
                    }
                });
    }

    private void modifyWorldRenderer(CtClass ctWorldRenderer) throws NotFoundException, CannotCompileException {
        ctWorldRenderer.getDeclaredMethod("updateRenderOrigin").insertAfter("{" +
                "distantrenderer.refreshDistantTreeCells();" +
                "}");
        ctWorldRenderer.getDeclaredMethod("refresh").insertAfter("{" +
                "distantrenderer.setAllOutdated();" +
                "}");
    }

    private void modifyCellRenderer() throws NotFoundException, CannotCompileException {
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.cell.CellRenderer",
                "tick",
                Descriptor.ofMethod(CtPrimitiveType.voidType, new CtClass[]{
                }),
                () -> (proxy, method, args) ->
                {
                    Object result = method.invoke(proxy, args);
                    tickAllVegetationCells();
                    return result;
                });
    }


    private void tickAllVegetationCells() {
        if (distantAreaCells != null) {
            try {
                lock.readLock().lock();
                for (DistantTreeCell distantAreaCell : distantAreaCells) {
                    if (distantAreaCell != null) {
                        distantAreaCell.tick();
                    }
                }
            } finally {
                lock.readLock().unlock();
            }
        }
    }

    @CallbackApi
    public boolean changeRenderDistance(String command, String[] parameters) {
        if (command.equalsIgnoreCase(RENDER_DISTANCE_COMMAND)) {
            try {
                if (parameters.length == 2) {
                    MAX_RENDER_DISTANCE = Integer.parseInt(parameters[1]);
                    printToConsole("Render distance for distant trees: " + MAX_RENDER_DISTANCE);
                } else {
                    printToConsole("Incorrect number of arguments supplied: " + RENDER_DISTANCE_COMMAND + " <Number of tiles>");
                }
                return true;
            } catch (NumberFormatException e) {
                printToConsole("Only numbers are possible for the render distance.");
                return true;
            }
        }
        return false;
    }

    @CallbackApi
    public void refreshDistantTreeCells() {
        if (distantAreaCells != null) {
            try {
                lock.readLock().lock();
                for (DistantTreeCell distantAreaCell : distantAreaCells) {
                    if (distantAreaCell != null) {
                        distantAreaCell.refresh();
                    }
                }
            } finally {
                lock.readLock().unlock();
            }
        }
    }


    @CallbackApi
    public void renderDistantTrees(Queue queue) {
        if (distantAreaCells != null) {
            try {
                lock.readLock().lock();
                for (DistantTreeCell distantAreaCell : distantAreaCells) {
                    if (distantAreaCell != null) {
                        distantAreaCell.renderTrees(queue);
                    }
                }
            } finally {
                lock.readLock().unlock();
            }
        }
    }

    @CallbackApi
    public void setAllOutdated() {
        if (distantAreaCells != null) {
            try {
                lock.readLock().lock();
                for (DistantTreeCell distantAreaCell : distantAreaCells) {
                    if (distantAreaCell != null) {
                        distantAreaCell.setOutdated();
                    }
                }
            } finally {
                lock.readLock().unlock();
            }
        }
    }


    @CallbackApi
    public void setupDistantVegetationCells(World world) {
        if (distantAreaCells == null) {
            try {
                lock.writeLock().lock();
                TextureManager.loadTextures(world);
                distantAreaCells = new ArrayList<>();
                for (int x = 0; x < world.getWorldSize(); x = x + 16) {
                    for (int y = 0; y < world.getWorldSize(); y = y + 16) {
                        DistantTreeCell distantAreaCell = new DistantTreeCell(world);
                        distantAreaCell.setCellPosition((int) Math.floor((float) (x * 4) / 64), (int) Math.floor((float) ((y * 4) / 64)));
                        distantAreaCells.add(distantAreaCell);
                    }
                }
            } finally {
                lock.writeLock().unlock();
            }
        }
    }

    private void printToConsole(String msg) {
        if (this.headupDisplay != null && msg != null && !msg.isEmpty()) {
            this.headupDisplay.consoleOutput(msg);
        }
    }

}
