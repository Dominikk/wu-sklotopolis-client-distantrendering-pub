package com.sklotopolis.mods.client.distantrendering;

import com.wurmonline.client.game.World;
import com.wurmonline.client.resources.textures.ResourceTexture;
import com.wurmonline.client.resources.textures.ResourceTextureLoader;

public class TextureManager {
    static ResourceTexture treeTexture;
    static ResourceTexture bushTexture;

    public static void loadTextures(World world) {
        treeTexture = ResourceTextureLoader.getTexture("img.terrain.treemap" + world.getSeasonManager().getSeasonAppendix());
        bushTexture = ResourceTextureLoader.getTexture("img.terrain.bushmap" + world.getSeasonManager().getSeasonAppendix());
    }
}
